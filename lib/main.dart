// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:matrimony/screen/login_page.dart';

void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: 'login_page',
    routes: {
      'login_page': (context) => Loginpage(),
    },
  ));
}

