// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:matrimony/database/detail.dart';

class ShowUser extends StatefulWidget {
  const ShowUser({Key? key, required this.user}) : super(key: key);
  final User user;

  @override
  State<ShowUser> createState() => _ShowUserState();
}

class _ShowUserState extends State<ShowUser> {

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
          title: Text(widget.user.name),
          centerTitle: true,
        ),
        body: Container(
          color: Colors.white,
          height: double.infinity,
          width: double.infinity,
          padding: EdgeInsets.all(14),
          child: Column(
            // mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text("ID :- ${widget.user.id}",
                  style: TextStyle(fontSize: 20, color: Colors.blue)),
              SizedBox(
                height: 15,
              ),
              Text("Name :- ${widget.user.name}",
                  style: TextStyle(fontSize: 20, color: Colors.blue)),
              SizedBox(
                height: 15,
              ),
              Text("Gender :- ${widget.user.gender}",
                  style: TextStyle(fontSize: 20, color: Colors.blue)),
              SizedBox(
                height: 15,
              ),
              Text("City :- ${widget.user.city}",
                  style: TextStyle(fontSize: 20, color: Colors.blue)),
              SizedBox(
                height: 15,
              ),
              Text("Description :- ${widget.user.description}",
                  style: TextStyle(fontSize: 20, color: Colors.blue)),
            ],
          ),
        ),
      ),
    );
  }
}
